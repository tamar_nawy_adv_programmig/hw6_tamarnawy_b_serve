﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace HW6_TamarNawy_B
{
    public partial class Form2 : Form
    {
        //define
        static class Constants
        {
            public const string PATH_LOG_IN = @"C:\Users\kfir\Desktop\user\Users.txt";
            public const string PATH_FILE_PER = @"C:\Users\kfir\Desktop\user\";

        }
        //Dictionary: key - date / value - name
        Dictionary<string, string> dataBase =
             new Dictionary<string, string>();
       
        //create calender
        GregorianCalendar theCalender = new GregorianCalendar();
        FileStream userFile;
        string theDateFromPicker;
        string fileUserPath;

        public Form2(string userName)
        {
            InitializeComponent();

            fileUserPath = Constants.PATH_FILE_PER + userName + "BD.txt"; 
                bool fileUserExist = File.Exists(fileUserPath);

                
                //if the file not exit create now
                if (fileUserExist.Equals(false))
                {

                    userFile = new FileStream(fileUserPath,
                           FileMode.Create,
                           FileAccess.Write);
                }
                //if the file exit open it to read
                if (fileUserExist.Equals(true))
                {
                  
                FileStream userFileRead = new FileStream(fileUserPath,
                        FileMode.Open,
                        FileAccess.Read);

                StreamReader reader = new StreamReader(userFileRead);

              
                string[] lines = System.IO.File.ReadAllLines(fileUserPath); //put the lines of the file in array
                foreach (string line in lines)
                {
                    if(!line.Equals(""))
                    {
                        string[] nameDate = line.Split(','); //split to name and date
                        string[] date = nameDate[1].Split('/'); //split the feilds of the date
                        dataBase.Add(nameDate[1], nameDate[0]); //key - date , value - name
                    }

                }

                


                userFileRead.Close();
                reader.Close();



            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        /*
         * This function will start when the date change
         */
        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
          //take the date that get
            string theSelect = monthCalendar1.SelectionRange.Start.ToShortDateString().ToString();

            //fit the date to the pattern M/D/YYYY
            string[] date = theSelect.Split('/');
            if(date[1].StartsWith("0"))
            {
                date[1] = date[1][1].ToString();
            }
            if (date[0].StartsWith("0"))
            {
                date[0] = date[0][1].ToString();
            }
            string goodDate = date[1] + '/' + date[0] + '/' + date[2];

            if (dataBase.TryGetValue(goodDate, out string myValue)) //get the right value
            {
                // use myValue;
                label3.Text = "חוגג/ת יום הולדת " + myValue + " בתאריך הנבחר ";
            }
           

        }
        /*
         * This function will start when user prase Add
         */
        private void button1_Click(object sender, EventArgs e)
        {
         
            if(userFile != null) //if the file is open
            {
                userFile.Close();
            }
            
            //open the file for appand
            userFile = new FileStream(fileUserPath,
                    FileMode.Append,
                    FileAccess.Write);


            string addName = textBox1.Text; //take the name to add

            StreamWriter writer = new StreamWriter(userFile);

            //fit the date to the pattern M/D/YYYY
            string[] date = theDateFromPicker.Split('/');
            if (date[1].StartsWith("0"))
            {
                date[1] = date[1][1].ToString();
            }
            if (date[0].StartsWith("0"))
            {
                date[0] = date[0][1].ToString();
            }
            string goodDate = date[1] + '/' + date[0] + '/' + date[2];

           // writer.WriteLine("\n");

            string lineWrite =addName + ',' + goodDate;

            writer.WriteLine(lineWrite); //write the good line to the file

            dataBase.Add(goodDate, addName); //add it to the dectinary

            writer.Close();
            userFile.Close();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            theDateFromPicker = dateTimePicker1.Value.ToShortDateString(); //take the date from the date picker
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        /*
         * This function will start when the user prase cancel
         */
        private void button2_Click(object sender, EventArgs e)
        {

            string message = "ביי ביי!";
            string caption = "ביטול";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;
            
            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.OK)
            {

                // Closes the form.

                this.Close();
               

            }
        }
    }
}
