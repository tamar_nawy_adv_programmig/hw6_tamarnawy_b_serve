﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace HW6_TamarNawy_B
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        //defines
        static class Constants
        {
            public const string PATH_LOG_IN = @"C:\Users\kfir\Desktop\user\Users.txt";
            public const string PATH_FILE_PER = @"C:\Users\kfir\Desktop\user\";

        }

        /*
         * This function will start when user prese enter
         */
        private void button1_Click(object sender, EventArgs e)
        {
           //Take the user name and the password
            string userName = textBox1.Text;
            string password = textBox2.Text;
            
            //open the file to read
            FileStream usersFile = new FileStream(Constants.PATH_LOG_IN, //THE PATH
                              FileMode.Open,
                              FileAccess.Read);


            StreamReader reader = new StreamReader(usersFile);

            string[] lines = System.IO.File.ReadAllLines(Constants.PATH_LOG_IN); //put all the lines in array

            bool good = false;

            //this loop will cheak if the user can log in
            foreach (string line in lines)
            {
               
                string inputStr = userName + "," + password;
                if (inputStr.Equals(line))
                {
                    good = true; //can log in
                    break;
                }
                
            }

            usersFile.Close();

            //if he cant come in
            if (good.Equals(false))
            {
                string message = "הקשת פרטים לא נכונים, אינך יכול להכנס!";
                string caption = "סיסמא שגויה";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;

                // Displays the MessageBox.

                result = MessageBox.Show(message, caption, buttons);

                if (result == System.Windows.Forms.DialogResult.OK)
                {

                    // Closes the parent form.

                    this.Close();

                }
            }

            if (good.Equals(true))
            {
                Form2 BirthdayForm = new Form2(userName); //open the second form

                BirthdayForm.Show();
            }

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e) //user name
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e) //password
        {
            
        }

        /*
         * This function will exit from the program
         */
        private void button2_Click(object sender, EventArgs e)
        {
            string message = "ביי ביי!";
            string caption = "ביטול";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;
            
            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.OK)
            {

                // Closes the parent form.

                this.Close();

            }
        }
    }
}
